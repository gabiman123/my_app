class ApplicationMailer < ActionMailer::Base
  default from: "toro270294@gmail.com"
  layout 'mailer'
end
