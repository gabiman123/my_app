class Post < ActiveRecord::Base
    has_many :comments
    belongs_to :user
    validates_presence_of :title, :message => "Tienes que poner un titulo"
    validates_length_of :body, :in=> 10..400, :message =>"Longitud no valida" 
end
