class PostsController < ApplicationController
	#protect_from_forgery with: :excep =>[:index,:create]
	protect_from_forgery :except => [:index, :create]
  before_action :set_post, only: [:show, :edit, :update, :destroy]

  # GET /posts
  # GET /posts.json
 # def index
  #    @posts = Post.all
  #end

  def index 
  	if admin_signed_in?
          @posts =Post.all
            respond_to do |format|
                format.json do
                    render :json => @posts, :callback => params[:callback]
                end
                format.html
            end
        end
      if user_signed_in?
          @posts = current_user.posts
            respond_to do |format|
                format.json do
                    render :json => @posts, :callback => params[:callback]
                end
                format.html
            end
        end
  end

  def show
  end

  # GET /posts/new
  def new
    @post = Post.new

  end

  # GET /posts/1/edit
  def edit
    user_signed_in?
      if cannot? :edit,@post
        flash[:alert] = "NO TIENES PERMISO PARA EDITAR"
          redirect_to :action =>'index'
      end
  end

  # POST /posts
  # POST /posts.json
  def create
    @post = Post.new(post_params)
    
    respond_to do |format|
      if @post.save
       # Correo.bienvenida(current_user).deliver_now
        flash[:success] = "El post se guardó correctamente"
        format.html { redirect_to @post }
        #format.json { render :show, status: :created, location: @asesoria }
          format.json do
            render :json => @post, :callback => params[:callback]
          end
      else
        #format.html { render :new }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    respond_to do |format|
      if @post.update(post_params)
        flash[:success] = "El post se modificò correctamente"
        format.html { redirect_to @post}
        format.json { render :show, status: :ok, location: @post }
      else
        flash[:error] = "El post no se pudo modificar"
        format.html { render :edit }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post.destroy
    respond_to do |format|
      flash[:success]="Se eliminò correctamente el post"
      format.html { redirect_to posts_url}
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
    	params.require(:post).permit( :title, :body, :categoria, :user_id) 
    end
end
